package sample;

import com.sun.prism.Graphics;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

class Ball {
    float x, y;
    float radius;
    float speedX, speedY;
    Color color;
    int width;
    int height;

    private final static Color DEFAULT_COLOR = Color.GREEN;


    public Ball(float x, float y, float radius, float speed, float angle, Color color) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.speedX = (float) (speed * Math.cos(Math.toRadians(angle)));
        this.speedY = -speed * (float) Math.sin(Math.toRadians(angle));


    }


    public Ball(float x, float y, float radius, float speed, float angle) {
        this(x, y, radius, speed, angle, DEFAULT_COLOR);
    }

    void update(Box box) {
        // Get the ball's bounds, offset by the radius of the ball
        float ballMinX = (float) box.minX + radius;
        float ballMinY = (float) box.minY + radius;
        float ballMaxX = (float) box.maxX - radius;
        float ballMaxY = (float) box.maxY - radius;

        // Calculate the ball's new position
        x += speedX;
        y += speedY;
        // Check if the ball moves over the bounds. If so, adjust the position and speed.
        if (x < ballMinX) {
            speedX = -speedX; // Reflect along normal
            x = ballMinX;     // Re-position the ball at the edge
        } else if (x > ballMaxX) {
            speedX = -speedX;
            x = ballMaxX;
        }
        // May cross both x and y bounds
        if (y < ballMinY) {
            speedY = -speedY;
            y = ballMinY;
        } else if (y > ballMaxY) {
            speedY = -speedY;
            y = ballMaxY;
        }
    }

    void draw(GraphicsContext context) {
        context.setFill(color);
        context.fillOval(x, y, radius, radius);
    }
}