package sample;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class Controller implements Initializable {

    @FXML
    public Canvas canvas;
    int currentBall = 0;
    GraphicsContext context;
    Ball[] balls = new Ball[20];
    private Box box;
    private double canvasWidth;
    private double canvasHeight;

    Parent root;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Random random = new Random();
        canvasWidth = canvas.getWidth();
        canvasHeight = canvas.getHeight();
        context = canvas.getGraphicsContext2D();
        box = new Box(0, 0, canvasWidth, canvasHeight, Color.WHITE);
        this.setup();

        canvas.setOnMouseClicked(mouseEvent -> {
            currentBall++;
            balls[currentBall] = new Ball((float) mouseEvent.getX(), (float) mouseEvent.getY(), random.nextInt(50),random.nextInt(30) , 5, Color.rgb(random.nextInt(255),random.nextInt(255),random.nextInt(255)));
            this.draw();
            System.out.println(balls[currentBall].x);

        });


    }

    private void draw() {


        for (Ball ball : this.balls) {
            if (ball != null) {
                ball.draw(context);
            }

        }
    }


    private void setup() {

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                update();
                redraw();
            }
        };

        timer.start();

    }

    private void update() {


        for (Ball ball : this.balls) {
            if (ball != null) {
                ball.update(box);

            }
        }
    }

    private void redraw() {
        box.draw(context);
        for (Ball ball : this.balls) {
            if (ball != null) {

                ball.draw(context);
            }
        }
    }

    public void setRoot(Parent root) {
        this.root=root;
    }
}
